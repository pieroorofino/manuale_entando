1. Piattaforma Entando - Concetti di base
================================================

1.1 Front End e Area di Amministrazione
------------------------------------------

 Entando comprende due distinte interfacce verso gli utenti:

 * il Front End: la parte pubblica, quindi ciò che vede l'utente che naviga sul web

 * l'Area di Amministrazione: il sistema a disposizione del proprietario/gestore del sito per la gestione delle funzionalità, configurazione, e redazione dei contenti informativi

 Nella versione attuale di Entando esistono numerose funzioni e scorciatoie per la gestione del sito
 che rendono meno evidente la divisione delle due interfacce, ma resta valida la divisione logica tra
 le due.
 Occorre tenere ben presente che Entando include un vero CMS (Content Management System), che
 ha fra i suoi obiettivi quello di raccogliere un set comune di informazioni che potranno essere
 divulgate attraverso differenti canali. Lo stesso contenuto informativo, ad esempio una notizia, può
 essere pubblicato in home-page, trovato nella lista dei risultati del motore di ricerca interno, e nel
 caso vi sia installati alcuni plug-in, fornito attraverso RSS, visualizzato su un dispositivo mobile
 attraverso il browser o una app, fornito ad altre applicazioni attraverso API di tipo REST eccetera.
 Per ottenere questi risultati occorre operare una completa separazione tra l'informazione pura e il
 modo in cui essa viene rappresentata. Infatti, la notizia dell'esempio precedente sarà vista in modo
 completamente diverso dagli utenti dei diversi canali. Non ci si deve meravigliare, quindi, se
 nell'area di amministrazione le informazioni appariranno diverse da come sono visualizzate sulle
 pagine web del sito: è la stessa differenza che c'è tra un articolo scritto a penna da un giornalista sul
 suo taccuino e lo stesso articolo pubblicato sulla pagine del quotidiano cartaceo.
 
1.2 Gli oggetti di Entando
----------------------------
 In questa sezione saranno presentati brevemente gli oggetti che costituiscono il mondo Entando e
 sarà spiegato in quale modo essi interagiscono per dare luogo ad un sistema web.

1.2.1 Le Pagine
^^^^^^^^^^^^^^^^^^^
 Le Pagine di Entando possono essere immaginate come dei contenitori vuoti, che dovranno essere
 "riempiti" con degli altri oggetti per dare luogo alle pagine web. Una pagina è definita attraverso un
 suo codice e un titolo.

1.2.2 L'Albero delle Pagine
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Le diverse Pagine sono collegate tra di loro in una struttura gerarchica: l'Albero delle pagine. L'albero
 delle pagine costituisce lo scheletro del sito o del portale web. La struttura dell'Albero riflette
 usualmente lo schema di navigazione del sito proposto agli utenti web.

  .. figure:: _static/images/alberoPagine.png
     :align: center
     :name: Albero delle Pagine
     :scale: 60 %

     Fig. 1 - Albero delle Pagine

1.2.3 I Modelli di Pagina
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Le pagine di un sito usualmente possiedono uno schema grafico omogeneo, con un piccolo numero
 di varianti. Si pensi per esempio in cui la struttura delle pagine differisce solamente tra home-page
 e pagina interna. In questo caso potremmo dire che son presenti due modelli di pagine (vedi Fig.
 2). Con questa logica, Entando prevede l'utilizzo dei Modelli di Pagina. Questi saranno realizzati
 dal web designer ed includono gli elementi decorativi ed uno schema per il posizionamento degli
 oggetti che popoleranno le pagine (vedi figura 3). I modelli di pagine sono utilizzati per la
 realizzazione di tutte le pagine del sito. Ogni modello può essere utilizzato per più pagine.

 .. figure:: _static/images/modelliPagina.png
    :align: center
    :scale: 50 %
    :name: Modelli di Pagina 2.2

    Fig. 2 - Modelli di Pagina 

 .. figure:: _static/images/strutturaModelli.png
    :align: center
    :scale: 50 %
    :name: Modelli di Pagina 2.3

    Fig. 3 - Modelli di Pagina

1.2.4 I Contenuti
^^^^^^^^^^^^^^^^^^^^^
 Il Contenuto, in Entando, è una unità di informazione strutturata, che potrà essere utilizzata in
 diverse forme e contesti, anche all'esterno del sito.
 I Contenuti sono raccolti in un Archivio di Contenuti, che è completamente indipendente dalle
 Pagine e dall'Albero delle Pagine. Non si deve mai confondere un Contenuto con una Pagina. Per
 ribadire questo concetto, anticipiamo che una Pagina può ospitare un Contenuto, molti Contenuti
 o anche nessun Contenuto, e che uno stesso Contenuto può essere pubblicato contemporaneamente su diverse Pagine in forme diverse, o anche su nessuna, pur restando
 disponibile agli utenti (ad esempio attraverso il motore di ricerca o gli RSS).
 Un altro importante concetto da ricordare è che un Contenuto comprende informazione pura e, al
 contrario, non comprende alcuna caratteristica di presentazione/decorazione (colori dimensioni,
 posizionamento) in quanto deve poter essere utilizzato in contesti in cui queste caratteristiche
 potrebbero non avere significato.

1.2.5 I Tipi di Contenuto
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 In uno stesso sito o portale possono coesistere diversi Tipi di Contenuto. Un Tipo di Contenuto è, in
 Entando, una classe di Contenuti caratterizzati da una stessa struttura. Esempi di Tipi di Contenuto
 possono essere la Notizia e l'Evento. La struttura di un Tipo di Contenuto è la definizione dei tipi di
 informazione elementare (Attributi) che lo costituiscono. Ad esempio, il Tipo di Contenuto Notizia
 può avere la seguente struttura:

 * Data (Data)
 * Autore (Testo monolingua)
 * Titolo (Testo multilingua)
 * Sottotitolo (Testo multilingua)
 * Sintesi (Testo multilingua)
 * Testo completo (Ipertesto multilingua)
 * Foto (Immagine)
 * Riferimenti (Lista di link)

 (è indicato tra parentesi il tipo di informazione di ogni Attributo).
 Il Tipo di Contenuto Evento può invece avere una Data Inizio, una Data Fine, una Località eccetera.
 La struttura di un Tipo di Contenuto può essere considerata una rappresentazione semantica, in
 quanto precisa il significato di ogni elemento di informazione che compone i contenuti.
 Come è facile intuire, ogni Contenuto nascerà sulla base della scelta di un Tipo di Contenuto e quindi
 ne erediterà la struttura e comprenderà il valore per ognuno degli attributi costituenti (nel caso di
 Attributi multilingua dovranno essere inseriti i valori nelle diverse lingue per uno stesso Attributo).
 Possono essere creati facilmente tutti i Tipi di Contenuto che sono necessari per la realizzazione del
 sito o portale. In più, Entando consente la modifica "a caldo" della struttura di un Tipo di Contenuto,
 anche se nell'Archivio sono già presenti Contenuti di quel Tipo.
 Gli Attributi che compongono la struttura di un Tipo di Contenuto devono essere scelti fra quelli
 messi a disposizione da Entando o dai suoi Plugin.

1.2.6 I Modelli di Contenuto
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Un Contenuto, come abbiamo visto, è costituito da un insieme di informazioni elementari che
 dovranno essere mostrate in modo diverso a seconda del canale di pubblicazione e del contesto.
 Per fare un esempio, una notizia potrà essere mostrata nella home-page di un ipotetico sito
 visualizzando solo il titolo e l'abstract, mentre in una pagina interna, raggiungibile tramite link, la
 stessa notizia sarà mostrata con titolo, sottotitolo, data, testo completo, foto e riferimenti (ma senza
 l'abstract). La notizia potrà anche essere trovata, anche se non più presente in home-page, tramite
 il motore di ricerca. In questo caso apparirà nella lista dei risultati della ricerca, rappresentata con
 la data, il titolo e l'abstract, per esempio. Quindi, la notizia è sempre la stessa ed è stata inserita una
 sola volta, ma Entando la visualizzerà diversamente a seconda del contesto. In ognuna delle
 rappresentazioni elencate, la Notizia potrà essere “formattata” utilizzando font eleganti, spaziature
 adatte, colori diversi per il titolo ed il testo e includendo eventuali altri elementi decorativi, come
 contorni, sfondi eccetera. Tutto ciò è descritto nei Modelli di Contenuto.
 I Modelli di Contenuto sono degli schemi di rappresentazione, realizzati solitamente dagli
 sviluppatori web e riutilizzabili durante l'aggiornamento quotidiano del sito. Ogni Modello di
 Contenuto è specifico per un solo Tipo di Contenuto, ma lo stesso Tipo di Contenuto (come la Notizia
 dell'ultimo esempio) può avere più Modelli di Contenuto differenti.
 Nel caso della pubblicazione su web, il Modello di Contenuto è realizzato in modo che il motore di
 renderizzazione di Entando lo traduca in un frammento di HTML (o una delle sue varianti) che
 insieme ad opportuni fogli di stile CSS produrrà il risultato desiderato.

1.2.7 Le Risorse
^^^^^^^^^^^^^^^^^^
 In Entando esistono due Attributi particolari: le Immagini e gli Allegati, che sono considerati
 "Risorse" con vita autonoma. Ciò significa che Immagini e Allegati possono essere caricati in un
 apposito Archivio di Risorse ed essere riutilizzati in più Contenuti (in altre parole, Immagini e Allegati
 non sono "incorporati" nei Contenuti bensì semplicemente "collegati" ai Contenuti).

1.2.8 Le Categorie
^^^^^^^^^^^^^^^^^^^^^^^^
 I Tipi di Contenuto determinano una differenziazione in base alla struttura interna dei Contenuti,
 ma non in base alle informazioni che contengono. Per organizzare i Contenuti presenti nell'archivio
 in base all'informazione da essi veicolata sono disponibili le Categorie.
 Le Categorie costituiscono un sistema di classificazione gerarchica libera applicabile sia ai Contenuti
 che alle Risorse. Ogni Contenuto o Risorsa può essere assegnato ad una o più Categorie. Le Categorie
 sono concettualmente indipendenti rispetto ai Tipi di Contenuto. Ad esempio, alla categoria
 denominata Giunta comunale possiamo associare sia una Notizia giunta comunale che un Evento
 giunta comunale.
 Le Categorie sono utilissime per effettuare, come vedremo, la pubblicazione selettiva di Contenuti
 omogenei.
 Osservazioni
 A questo punto osserviamo che l'attività di popolamento di un Archivio di Contenuti da parte di una
 redazione può essere completamente indipendentemente dalle attività di pubblicazione sulle
 pagine del sito. Il CMS di Entando consente, al limite, di popolare e tenere aggiornato un archivio di
 contenuti destinati esclusivamente ad un canale RSS o ad una app per smartphone, per esempio,
 senza che esistano pagine web. Un utente che deve svolgere solo attività di redazione dei contenuti
 potrebbe fermarsi qui.
 E se invece vogliamo pubblicare i Contenuti sulle Pagine web del sito? Basta usare i Widget,
 ovviamente, come indicato nei prossimi paragrafi.

1.2.9 I Widget di Entando
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 I Widget sono un'astrazione peculiare di Entando e servono a rappresentare un qualunque elemento
 informativo o funzionale da pubblicare all'interno delle Pagine.
 I Widget hanno, come le portlet di altri software, la possibilità di essere pubblicati sulle Pagine in
 determinate posizioni, ma possono assumere un carattere diverso ed essere, ad esempio,
 rappresentati come Widget mobili.
 La piattaforma Entando include al suo interno la definizione di un certo set di Widget direttamente
 impiegabili all'interno del portale oppure se ne possono definire di nuovi a partire dai Widget
 esistenti mediante configurazione. Inoltre, durante la realizzazione del portale o in un suo successivo
 sviluppo, si possono creare nuovi Widget. Ulteriori Widget possono essere disponibili attraverso i
 Plugin di Entando.
 I Widget sono oggetti assolutamente generali che, virtualmente, possono svolgere qualunque
 funzione (purché qualcuno li realizzi). La logica di base per la pubblicazione di un Widget su una
 Pagina è molto semplice:

 * si sceglie la Pagina
 * si sceglie la posizione (definita nel Modello di Pagina) su cui si vuole pubblicare il Widget
 * si sceglie il Widget fra quelli disponibili nel sistema
 * si completa la parametrizzazione del Widget (quando necessario) impostando le informazioni richieste

 Esistono poi delle scorciatoie per semplificare ulteriormente l'operazione.
 Osservazioni:
 Con gli oggetti visti fin qui si può realizzare qualunque sito o portale web. Infatti:

 * l'Albero delle Pagine consente una articolazione di pagine comunque complessa
 * i Widget consentono di includere nelle pagine qualunque informazione o servizio

 Resta quindi da vedere, come ci si aspetta, quali sono i Widget disponibili. Quelli più importanti e
 più utilizzati, sono i Widget collegati al CMS di Entando. Ma prima andiamo a vedere gli oggetti del
 CMS (contenuti – tipi di contenuto – modelli di contenuto).

1.2.10 Widget per la pubblicazione di un Contenuto
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Come abbiamo visto nella prima parte, Entando prevede per la pubblicazione di informazioni e di
 servizi due soli tipi di oggetto: le Pagine (contenitori) e i Widget (da inserire nei contenitori). Quindi,
 per pubblicare qualunque cosa dobbiamo disporre di un Widget che faccia da intermediario.
 Per pubblicare staticamente uno specifico Contenuto si usa il Widget "Pubblicazione Contenuto".
 Come ci si può aspettare, dopo aver scelto la Pagina e la posizione della Pagina su cui si intende
 pubblicare il Contenuto, occorre specificare:

 * qual è il Contenuto da pubblicare
 * qual è il Modello di Contenuto da utilizzare per rappresentarlo

 Niente altro, lo specifico contenuto resterà pubblicato nella pagina finché non sarà esplicitamente
 rimosso o sostituito.

1.2.11 Widget per la pubblicazione di una Lista di Contenuti
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Spesso capita di pubblicare un lista dinamica (elencazione) di contenuti, come può essere la lista
 delle ultime notizie o di prossimi eventi.
 Entando fornisce un Widget apposito, che consente di definire i criteri di selezione e ordinamento
 dei contenuti da pubblicare. Il Widget comprende anche gli strumenti di "paginazione" per mostrare
 solo pochi elementi per volta nei casi in cui la lista sia troppo lunga per essere visualizzata
 interamente.
 In questo caso, la configurazione del Widget comprende numerose opzioni, consentendo di:

 * scegliere il Tipo di Contenuto
 * filtrare i contenuti in base alle Categorie o ai valori di alcuni suoi attributi
 * ordinare i contenuti in base ai valori di attributi
 * scegliere il Modello di Contenuto da utilizzare nella rappresentazione in lista

 Notare che la lista è generata dinamicamente. Quindi, se la situazione dei contenuti nell'archivio
 cambia, la lista è aggiornata automaticamente. Ad esempio, se si configura una lista per visualizzare
 le ultime notizie, non appena viene inserita in archivio una nuova notizia, questa è immediatamente
 inclusa nella lista visibile su web (previa approvazione, come vedremo nei paragrafi successivi).

1.2.12 Utenti, Gruppi e autorizzazioni
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Entando utilizza un modello consueto di definizione degli Utenti e delle loro autorizzazioni, basato
 sui Ruoli, ed anche uno più originale relativo ai Gruppi.
 Un Utente registrato è riconosciuto dal sistema attraverso le sue credenziali di accesso e può essere
 autorizzato a compiere operazioni mediante l'assegnazione di uno o più Ruoli. Un **Ruolo** non è altro
 che un insieme di autorizzazioni elementari (ad esempio: permesso di accesso all'Area di
 Amministrazione, di effettuare redazione di Contenuti, di creare e modificare Pagine). L'Utente avrà
 quindi tutti i permessi compresi nei Ruoli a lui associati.
 I permessi compresi nei Ruoli sono permessi di accesso a “funzioni” (*cosa posso fare*). A questo
 modello di permesso, Entando ne affianca un altro, quello di permesso sugli "oggetti" del sistema
 (*su cosa posso operare*). Questo secondo tipo di permesso passa attraverso il concetto di **Gruppo**. Il
 concetto base è il seguente:

 * sia gli Utenti che gli Oggetti possono avere un Gruppo associato
 * un Utente può operare sugli oggetti che appartengono ad uno dei Gruppi a lui associati
 
 Un esempio per chiarire. Supponiamo in un sito siano definiti quattro gruppi: "*Nord*", "*Est*", "*Sud*",
 "*Ovest*". Supponiamo che ogni Contenuto del sito sia associato ad uno solo di questi quattro Gruppi.
 Un Utente associato al gruppo "*Nord*" potrà vedere tutti e soli i contenuti associati allo stesso
 gruppo. Un Utente assegnato ai gruppi "*Nord*" e "*Ovest*" potrà vedere i contenuti di entrambi tali
 gruppi, un altro utente potrà avere assegnati i gruppi "*Nord*" e "*Est*" e vedrà i relativi contenuti.
 E' importante notare che i permessi collegati ai Ruoli e quelli collegati ai Gruppi si "intersecano",
 consentendo una definizione fine delle autorizzazioni. Ad esempio, è possibile autorizzare un Utente
 a fare la sola redazione dei contenuti di un certo gruppo, un altro a fare supervisione sui contenuti
 di due gruppi, un altro ancora a modificare le Pagine di tutti i gruppi e così via.
 Alcuni oggetti di Entando consentono una configurazione ancora più fine. In particolare, ogni
 Contenuto o Pagina hanno un "Gruppo Proprietario" e uno o più "Gruppi in Visualizzazione". Gli
 utenti associati allo stesso Gruppo che ha la proprietà dell'oggetto potranno modificarlo, gli utenti
 associati ad un dei Gruppi di visualizzazione potranno solo vederlo.

1.2.13 Processi di Redazione e Approvazione dei Contenuti
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 In questo paragrafo sarà illustrato sinteticamente il processo secondo il quale i Contenuti saranno
 inseriti e pubblicati sul sito o portale realizzato con Entando. Per chiarezza, si farà riferimento ad
 una particolare sequenza di operazioni, che però può avere molte varianti, anche in base alle
 autorizzazioni degli operatori. Lo scopo principale è quello di illustrare la differenza tra un contenuto
 "Approvato" e uno "Non Approvato".
 Il processo inizia con la redazione di un nuovo Contenuto. Al momento del salvataggio nell'archivio,
 il redattore salva il Contenuto in stato "Non Approvato". Ciò significa che il Contenuto è presente in
 Archivio e può essere consultato dagli altri operatori autorizzati, ma non sarà in alcun modo
 disponibile agli utenti del Front End.
 In un momento successivo, un supervisore accede allo stesso Contenuto, eventualmente lo modifica
 e poi lo salva in stato "Approvato". A questo punto, quel Contenuto è virtualmente (e di solito anche
 effettivamente) disponibile per essere visualizzato dagli utenti del Front End. Ad esempio, se il
 contenuto approvato è una Notizia ed esiste un Widget che pubblica la lista delle ultime notizie sulla
 home-page, quel contenuto sarà automaticamente e immediatamente pubblicato. Lo stesso
 contenuto potrà anche comparire nei risultati di una ricerca attraverso il motore di ricerca.
 Successivamente, il redattore accede nuovamente allo stesso contenuto (che è "Approvato"), lo
 modifica e lo salva ancora in stato "Non Approvato". In questa situazione, il contenuto è ancora
 visibile sul Front End nella versione precedentemente approvata, mentre l'ultima versione resta in
 *stand by* nell'archivio dei contenuti (in attesa di approvazione). In questo particolare stato, il
 contenuto esiste, in pratica, in due versioni: una approvata e visibile in Front End, una in attesa di
 approvazione. A seguito di un nuova "approvazione" da parte di un supervisore, la nuova versione
 diventerà effettivamente visibile al pubblico.
 Se il supervisore decide che il contenuto non deve più essere visibile in alcun modo nel sito, lo porta
 nuovamente nella situazione "Non Approvato" (in Entando, il passaggio da Approvato a Non
 Approvato è chiamato "sospensione"). Quando il contenuto è in tale situazione, può anche decidere
 di cancellarlo fisicamente e definitivamente dall'archivio dei contenuti.
 A questo punto occorre una precisazione: Entando controlla che in qualunque istante le
 informazioni siano in uno stato consistente. Per dare tale garanzia, è possibile che certe operazioni
 siano impedite in determinate condizioni. Ad esempio, non è possibile cancellare dall'archivio un
 contenuto "Approvato". E' altresì impossibile sospendere (portare in stato "Non Approvato") un
 contenuto che è stato pubblicato tramite un Widget di pubblicazione di un contenuto (se fosse
 consentito, sulla pagina di pubblicazione apparirebbe un "buco" di informazione).
