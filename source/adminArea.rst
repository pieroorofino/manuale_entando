2. Area di Amministrazione - Guida alle funzioni.
###################################################

2.1 Area di Amministrazione
===============================
 L'area di amministrazione di Entando comprende tutte le funzionalità necessarie per l'aggiornamento, in fase di esercizio, del portale, delle informazioni e dei servizi da esso esposti.
 L'accesso è consentito tramite autenticazione (login) e le funzionalità utilizzabili possono essere
 limitate in base alle autorizzazioni concesse all'utente.
 Nei capitoli seguenti sono descritte le diverse funzionalità.
 L'accesso all'area di amministrazione è consentito esclusivamente agli utenti registrati e autorizzati.
 Nell'area di amministrazione saranno accessibili (e visibili) solo le voci di menù consentite dai
 permessi assegnati dal proprio ruolo.
 Quando si effettua l’accesso all'area di amministrazione:
 Se l’utente non appartiene ad un gruppo specifico, non potrà avere accesso a nessuno degli elementi
 (pagine, contenuti e risorse) del sistema che i gruppi rendono disponibili.
 Se l’utente appartiene ad un gruppo specifico, potrà operare (in maniera subordinata ai permessi
 posseduti dal proprio ruolo) sulle pagine, sui contenuti ed sulle risorse proprie del gruppo di
 appartenenza.
 Se l’utente appartiene a diversi gruppi specifici, potrà operare all’interno di ognuno dei gruppi di
 appartenenza accedendo (in maniera subordinata ai permessi posseduti dal proprio ruolo) alle
 pagine, ai contenuti ed alle risorse propri di questi gruppi.
 
2.2 Login
=================
 Attraverso il form di login è possibile autenticarsi al portale e usufruire delle funzionalità in base alle
 autorizzazioni assegnate all'utente dall'amministratore del sistema.
 Inserire Username e Password relativi all'account, selezionare la lingua e cliccare sul pulsante LOG IN.

 .. figure:: _static/images/login.png
    :align: center
    :scale: 60 %
    :name: Modelli di Pagina

    Fig. 4 - Interfaccia di Login

2.3 Pagina Iniziale
========================
 La figura seguente mostra l'homepage dell'area di amministrazione di Entando.
 Nella parte centrale della homepage sono disponibili gli indicatori sullo stato delle pagine, dei
 contenuti, la liosta dei contentui e delle pagine presenti all’interno delèprtale. La configurazione
 della dashboard può cambiare in base ai permessi dell’utente logato.
 Nella colonna destra, sotto il profilo utente, è disponibile la sezione “Setting rapido”, per consentire
 un accesso rapido alle funzioni più utilizzate dal CMS.

 .. figure:: _static/images/dashboard.png
    :align: center
    :scale: 40 %
    :name: Pagina iniziale Area di Amministrazione

    Fig. 5 - Pagina iniziale Area di Amministrazione

2.3.1 Profilo Utente
-----------------------
 In alto a destra, è presente un piccolo menù con alcune funzioni:

 * “Profilo” consente di accedere alla pagina del profilo per modificare la password ed altre eventuali informazioni dell'utente collegato
 * “Vai al portale”: consente di accedere alla home page del portale, mantenendo lo stato di autenticazione dell'utente
 * “Esci”: effettua il *logout* dell'utente dall'area di amministrazione

 .. figure:: _static/images/profiloUtente.png
    :align: center
    :name: Profilo Utente

    Fig. 6 - Profilo utente

2.4 Menù principale
===========================
 Sulla destra della homepage dell'area di amministrazione è disponibile il “Menù” che consente di
 accedere alle diverse voci del menù:

 .. figure:: _static/images/menu.png
    :align: center
    :scale: 65 %
    :name: Menù 

    Fig. 7 - Menù

2.5 Contenuti
=======================
 Le funzionalità di gestione dei contenuti sono accessibili dalla voce di Menù App > CMS > Contenuti
 Presenta la pagina contenente la lista dei contenuti presenti nel portale e le funzionalità per la
 gestione dei contentui, le funzionalità reviste sono

 * Creazione di nuovi contenuti
 * Ricerca contenuti
 * Approvazione contentui
 * Messo on hold di contenuti

 .. figure:: _static/images/contenuti.png
    :align: center
    :scale: 50 %
    :name: Contenuti

    Fig. 8 - Contenuti



 Dal punto di vista della redazione, un Contenuto comprende due gruppi di informazioni:
 Informazioni generali, “accessorie”, identiche per qualunque tipo di Contenuto, e comprendono:
 
 * una descrizione “interna” del contenuto (visibile solo ai redattori in area di amministrazione)
 * il Gruppo Proprietario del Contenuto
 * uno “stato” utile come promemoria
 * la lista dei “Gruppi in sola visualizzazione” (i Gruppi i cui utenti potranno vedere ma non modificare il contenuto)
 * le Categorie associate al contenuto, per la sua classificazione

 Attributi:

 * Sono gli elementi che costituiscono il vero contenuto informativo, e hanno diversa struttura a seconda del tipo di contenuto. Nelle funzioni di redazione sono mostrati in raggruppamenti corrispondenti a ciascuna delle lingue configurate nel sistema.

2.5.1 Lista Contenuti
-------------------------
 La pagina Lista Contenuti è divisa in due parti principali:
 
 * le funzioni di ricerca (parte alta)
 * la lista dei Contenuti (parte bassa)

 .. figure:: _static/images/listaContenuti.png
    :align: center
    :scale: 65 %
    :name: Lista contenuti

    Fig. 9 - Lista contenuti - sezioni form di ricerca

 .. figure:: _static/images/risultatiRicerca.png
    :align: center
    :name: Risultati ricerca

    Fig. 10 - Lista contenuti - risultati ricerca

 Inizialmente, la lista comprende tutti i Contenuti presenti nell'archivio, ordinati per data di ultima  modifica.
 La lista ha forma di tabella, con le seguenti informazioni per ogni contenuto:

 * Descrizione (la descrizione interna del contenuto)
 * Autore
 * Codice
 * Tipo Contenuto
 * Stato del contenuto
 * Operazioni

 Si ricorda che quando il contenuto è nello stato Approvato ma con modifiche, la versione visibile
 pubblicamente è diversa da quella modificata visibile attraverso le funzioni di redazione.
 La tabella mostra dieci risultati per volta. In basso sono presenti gli strumenti per consultare le
 ulteriori pagine di risultati.

2.5.1.1 *Operazioni sui Contenuti*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 A partire dalla lista dei contenuti è possibile eseguire diverse operazioni (immediatamente al di
 sotto, vedi fig. 11):

 * Modifica Contenuto
 * Copia-Incolla Contenuto
 * Versione bozza (versione di lavoro)
 * Versione pubblicata (versione pubblica)

 .. figure:: _static/images/operazioniContenuto.png
    :align: center
    :scale: 65 %
    :name: Operazioni Contenuto

    Fig. 11 - Operazioni su contenuto

 Questi quattro comandi operano sul solo contenuto evidenziato. In testa alla tabella si trovano poi
 i pulsanti per effettuare altre operazioni che operano su tutti i contenuti della lista che sono stati
 selezionati (checkbox attivo):

 * Approva
 * Sospendi
 * Elimina

2.5.1.1.1 Modifica Contenuto
""""""""""""""""""""""""""""""""""
 Questa funzione consente di modificare una Contenuto precedentemente salvato. La fase di
 redazione è identica a quella descritta nel caso di creazione di un nuovo Contenuto (esclusa la fase
 preliminare di scelta del tipo di contenuto).
 Si ricorda che se il contenuto è già stato precedentemente approvato, questa funzione opera su una
 versione distinta da quella visibile pubblicamente. La successiva nuova Approvazione farà si che la
 versione di lavoro diventi pubblica.

2.5.1.1.2 Copia-Incolla Contenuto
""""""""""""""""""""""""""""""""""""""""
 Questa funzione crea una copia del Contenuto evidenziato e porta alla funzione di modifica della
 nuova copia. Il nuovo Contenuto esisterà a tutti gli effetti solo dopo un nuovo salvataggio.

2.5.1.1.3 Ispeziona Contenuto
""""""""""""""""""""""""""""""""""""""
 Questa funzione esiste in due varianti, che consentono di esaminare i dettagli di un contenuto
 rispettivamente nella versione di lavoro e nella versione pubblica. La funzione consente la sola
 consultazione delle informazioni.
 Le informazioni rese disponibili sono le seguenti:

 * dati generali del contenuto
 * gruppo proprietario e gruppi di sola visualizzazione
 * categorie associate
 * referenze (contenuti e pagine a cui fanno riferimento i link presenti in questo contenuto)
 * referenze esterne (contenuti comprendenti link che fanno riferimento a questo contenuto;
 * pagine che pubblicano questo contenuto)
 * il contenuto informativo vero e proprio per ogni lingua presente nel sistema

 Le informazioni sulle “referenze” sono molto importanti, in quanto la presenza di collegamenti logici
 fra oggetti può determinare l'impossibilità di effettuare certe operazioni (ad esempio, cancellare un
 contenuto pubblicato su una pagina).

2.5.1.1.4 Approvazione Contenuti
"""""""""""""""""""""""""""""""""""""""
 Questo strumento consente di effettuare l'Approvazione di uno o più contenuti direttamente dalla
 lista, senza entrare nella funzione di redazione.
 Selezionare uno o più contenuti con un clic sulla descrizione, poi fare clic sullo strumento Approva.

2.5.1.1.5 Sospensione Contenuti
"""""""""""""""""""""""""""""""""""""
 Questo strumento consente di effettuare la Sospensione (revoca della Approvazione) di uno o più
 contenuti direttamente dalla lista, senza entrare nella funzione di redazione.
 Selezionare uno o più contenuti con un clic sulla descrizione, poi fare clic sullo strumento Sospendi.

2.5.1.1.6 Eliminazione Contenuti
"""""""""""""""""""""""""""""""""""""""
 Questo strumento consente di eliminare uno o più Contenuti. I Contenuti Approvati non possono
 essere eliminati.
 Selezionare uno o più contenuti con un clic sulla descrizione, poi fare clic sullo strumento Elimina.

2.5.1.1.7 Ricerca Contenuti
""""""""""""""""""""""""""""""""
 Si tratta di una funzione destinata al reperimento dei contenuti in archivio, di supporto alle attività
 di redazione e gestione dei contenuti nell'area di amministrazione. Questa funzione non ha niente
 a che vedere con il Motore di Ricerca destinato al front-end.
 La funzione di ricerca è presente sulla pagina Lista Contenuti, nella parte superiore. Lo strumento è
 piuttosto articolato e può essere utilizzato a diversi gradi.
 Il primo livello consiste nella semplice ricerca di una porzione di testo nelle descrizioni dei contenuti. Per effettuare la ricerca è sufficiente digitare una porzione di testo nel campo “Cerca per
 descrizione” e premere il pulsante “Cerca”. I risultati saranno presentati nella tabella sottostante, già descritta nel paragrafo “Lista Contenuti”.
 Per una ricerca più selettiva si possono usare gli strumenti della sezione “Raffina la Ricerca”, che viene visualizzata con un clic sul triangolo a fianco della didascalia. Gli strumenti disponibili sono:
 
 **Tipo:** consente di limitare la ricerca ai contenuti di un solo Tipo scelto fra quelli esistenti (e di attivare
 eventuali ulteriori campi, come spiegato più avanti)
 
 **Categoria:** consente di limitare la ricerca ai contenuti associati ad una Categoria
 
 **Codice:** ricerca il contenuto in base al suo Codice univoco
 
 **Gruppo:** limita la ricerca ai contenuti di un gruppo
 
 **Stato:** limita la ricerca ai contenuti con lo stato selezionato
 
 **Approvazione:** consente di ricercare i soli contenuti approvati o non approvati
 
 Se si sceglie uno specifico **Tipo**, saranno messi a disposizione anche i campi per la ricerca su alcuni
 degli attributi di questo specifico tipo (gli attributi dichiarati “ricercabili” nel tipo di contenuto, come
 spiegato nella relativa sezione). 
 Attenzione: tutti i parametri di ricerca inseriti operano in modo da restringere la ricerca; i risultati
 della ricerca sono cioè i contenuti che soddisfano contemporaneamente tutti i criteri di ricerca
 specificati (condizioni in *AND*).
 Oltre a raffinare la ricerca, è possibile decidere quali informazioni visualizzare nella tabella dei
 risultati, cliccando sulla sezione “Aggiunte alla tabella dei risultati”. E' sufficiente selezionare i
 checkbox corrispondenti alle informazioni desiderate, prima di premere il pulsante “Cerca”.

2.5.2 Nuovo Contenuto
--------------------------------
 Premendo sul menù “Nuovo” attraverso il menù a tendina è necessario scegliere il **Tipo di
 Contenuto** fra quelli già configurati e disponibili che si vuole creare.
 I primi dati richiesti sono i seguenti che fanno parte delle informazioni generali:

 **Descrizione:** descrizione interna del contenuto, se non inserita verrà utilizzato il titolo come
 descrizione

 **Gruppo proprietario:** da scegliere fra quelli cui l'utente è associato

 .. figure:: _static/images/nuovoContenuto.png
    :align: center
    :scale: 65 %
    :name: Operazioni Contenuto: dati

    Fig. 12 - Nuovo contenuto: dati iniziali

 Nella parte centrale della pagina compare l’interfaccia che comprende il contenuto vero e proprio
 composto dagli Attributi. Nella schermata compaiono diversi tab, uno per ogni lingua configurata
 nel sistema.
 Queste sezioni comprendono tutti i campi e gli strumenti per l'inserimento delle informazioni
 specifiche del contenuto (attributi del contenuto). La compilazione dei diversi tipi di attributo, pur
 essendo ragionevolmente intuitiva, è illustrata in dettaglio in un apposito successivo paragrafo.

  .. figure:: _static/images/esempioTipiContenuto.png
    :align: center
    :name: Esempio Contenuto

    Fig. 13 - Nuovo contenuto: esempio tipo di contenuto 

 Concetti generali relativi al multilinguismo:
 Alcuni attributi sono “mono-lingua”; la compilazione di questi attributi è possibile solo nel primo
 tab, quello della lingua di default.
 Alcuni attributi sono multilingua e richiederanno la compilazione differenziata in ciascuna lingua
 (nei relativi tab).
 Informazioni sugli attributi: alcuni attributi presentano, a fianco del nome, un'icona “lampadina”
 che consente la visualizzazione di informazioni accessorie sull'attributo:
 
 * Obbligatorio (il campo deve essere compilato obbligatoriamente)
 * Utilizzabile come filtro nelle liste (significa che sarà possibile utilizzare questo attributo per filtrare i contenuti nella visualizzazione in lista, nel contesto dell'apposito Widget, e in casi
   similari)
 * Ricercabile: per gli attributi che comprendono un testo, significa che questo sarà preso in considerazione nelle ricerche full text del motore di ricerca

 Attributi multipli: quasi tutti gli attributi possono essere presenti sotto forma di “liste” per
 l'inserimento di valori multipli (nessun valore, un valore o più valori). Ciò è chiaramente indicato a
 fianco del nome dell'attributo (dicitura “Lista”). Per inserire un nuovo valore di attributo in una lista
 occorre prima premere il pulsante “Aggiungi” e poi procedere alla compilazione (eventualmente
 guidata nei casi più complessi).

2.5.2.1 *Compilazione degli Attributi di un Contenuto*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Gli Attributi sono i componenti di informazione elementari che compongono un Contenuto e
 possono essere di diverso tipo (testo, data, link, immagine eccetera) e con diverse caratteristiche.
 Nel seguito si forniscono ulteriori spiegazioni e le istruzioni per la compilazione.

2.5.2.2 *Attributi testuali*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Gli attributi che ospitano un testo semplice (più o meno lungo) o un numero si presentano come
 semplici caselle di testo da compilare normalmente. Se sono multi-lingua, occorrerà effettuare la
 compilazione in ognuna delle lingue configurate.

2.5.2.3 *Attributo Data*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Questi attributi possono essere compilati mediante digitazione, secondo lo schema indicato, ma
 comprendono anche uno strumento (calendario) per la compilazione rapida.

2.5.2.4 *Attributi Enumerato e Booleano*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Sono attributi per i quali si deve scegliere un valore tra quelli proposti. Si possono presentare come
 liste a tendina o “check box” o “radio button”.

2.5.2.5 *Attributi Compositi*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Sono costituiti dalla combinazione di due o più attributi più semplici, e compaiono in lista. Il risultato
 è una sorta di tabella di dati. Valgono le regole di compilazione degli attributi semplici che lo
 compongono più le regole di compilazione della Lista.

2.5.2.6 *Attributo Ipertesto*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 onsente l'inserimento di un testo con gli effetti base di caratterizzazione (grassetto, sottolineato,
 corsivo) e soprattutto con hyperlink. Per l'inserimento è disponibile un semplice e intuitivo editor.
 Per informazioni sull'inserimento degli hyperlink nel testo si veda anche il paragrafo dell'Attributo
 Link.

2.5.2.7 *Attributo Link*
^^^^^^^^^^^^^^^^^^^^^^^^^^
 In Entando, un Link è la rappresentazione di un usuale hyperlink. Esistono però tre diverse
 connotazioni del Link: link esterno, link a Contenuto, link a Pagina. Il link esterno è in tutto e per
 tutto un URL che fa riferimento a una risorsa esterna del web. I Link a Contenuto e a Pagina si
 riferiscono invece a oggetti interni al sito e, in quanto riconosciuti dal sistema, consentono una
 gestione comoda e sicura. In particolare, il Link a Pagina ha come destinazione una Pagina del
 sistema; quando lo si definisce è sufficiente selezionare la Pagina di destinazione fra quelle esistenti,
 senza scrivere nessun URL. Il link a Contenuto ha come destinazione un Contenuto, e vale quanto
 detto nel caso precedente. Mentre il link a Pagina ha una destinazione certa, la Pagina appunto, il
 link al Contenuto ha una destinazione che viene elaborata dal sistema in modo da raggiungere la
 destinazione (il Contenuto scelto) in qualunque caso (anche se il contenuto non è stato pubblicato).
 Durante la compilazione dell'attributo Link viene dapprima richiesta la scelta del tipo di Link (fra i
 tre tipi illustrati) e, in un passo successivo, viene richiesta la destinazione: nel caso di URL esterno la
 si dovrà digitare, nel caso di Link a Pagina o a Contenuto viene proposta l'appropriata funzione di
 ricerca e selezione (per i dettagli sulla ricerca dei contenuti vedere l'apposito capitolo).

2.5.2.8 *Attributo Immagine*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 L'attributo di tipo Immagine può essere inserito scegliendo un'immagine dall'Archivio Immagini o
 caricandone una al momento (seguendo il link “Nuova Risorsa”). Per informazioni sulla ricerca e
 caricamento di immagini vedere l'apposito capitolo.
 Dopo la scelta dell'immagine, sarà possibile inserire il testo associato, che costituisce una breve
 descrizione utile in particolare come testo alternativo per chi ha carenze visive.
 Per sostituire l'immagine già precedentemente caricata nell'attributo occorre dapprima eliminare
 quella esistente e poi effettuare il caricamento della nuova.

2.5.2.9 *Attributo Allegato*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^ 
 Valgono le stesse indicazioni di caricamento illustrate per l'attributo immagine.

2.5.2.10 *Anteprima del contenuto*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Dopo aver compilato il contenuto, anche parzialmente, è possibile visionare l'Anteprima, per vedere
 come il contenuto apparirà sulla pagina web nella forma “vestita”, senza necessità di salvare
 preliminarmente il contenuto.
 Nei tab “lingua” è presente la lista di scelta “Pagina per l'anteprima” e il pulsante Anteprima. Le
 pagine di anteprima sono predisposte in fase di realizzazione del sito e consentono, se predisposte,
 di vedere come il contenuto apparirà in diversi contesti.
 Nella pagina di Anteprima è chiaramente indicato un pulsante per ritornare alla fase di redazione
 del contenuto.

2.5.2.11 *Informazioni sul Contenuto*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Questa scheda comprende le seguenti informazioni:

 **Gruppi di sola visualizzazione:** se il Gruppo Proprietario è uno dei gruppi riservati, è possibile dare
 comunque visibilità al Contenuto anche agli utenti di altri gruppi (che non potranno modificarlo). E'
 possibile associare un qualsiasi numero di gruppi di sola visualizzazione selezionandoli uno alla volta
 dalla lista e premendo “Associa”. I gruppi associati sono elencati e possono essere eliminati usando
 l'icona “-”.

 **Gestione Categorie:** in questa sezione è possibile associare una o più Categorie al contenuto,
 selezionandole dall'albero delle Categorie e associandole con il pulsante “Associa”. Le categorie
 associate saranno visualizzate in una tabella (sotto l'albero delle categorie) che comprende anche
 l'icona per l'eliminazione di ogni categoria precedentemente associata
 
 .. figure:: _static/images/gruppi.png
    :align: center
    :name: Nuovo contenuto: esempio tipo di contenuto

    Fig. 14 - Nuovo contenuto: esempio tipo di contenuto  

2.5.2.12 *Azioni*
^^^^^^^^^^^^^^^^^^^^^^^^
 Questa sezione può comprendere due pulsanti di salvataggio del contenuto e un pulsante sullo stato
 del contenuto:

 **“Salva e Continua”:** consente di salvare il contenuto senza approvarlo

 **“Salva e Approva”:** questo pulsante è disponibile solo se l'utente ha l'autorizzazione per approvare i contenuti (tipicamente quella di supervisore dei contenuti).
 
 **Stato:** selezionabile dalla lista

 .. figure:: _static/images/azioniSuContenuto.png
    :align: center
    :name: Azioni su nuovo contenuto

    Fig. 15 - Azioni su nuovo contenuto

 Attenzione: il contenuto non viene salvato finché non si preme uno di tali pulsanti. Durante la
 compilazione, se è di lunga durata, è opportuno salvare frequentemente il contenuto. Occorre però
 che il contenuto sia in uno stato consistente (ad esempio, campi obbligatori compilati), altrimenti
 non potrà essere salvato.

2.6 Risorse
==============
 Comprende le funzioni per la gestione delle **Immagini** e degli **Allegati**. Entrambi questi tipi di oggetto
 dispongono di un archivio indipendente. Sia le Immagini che gli Allegati non possono essere
 pubblicati direttamente, ma possono essere inclusi in un contenuto.

2.6.1 Immagini
--------------------
 Le Immagini in Entando sono originate da file in formato JPEG o PNG. Al momento del caricamento
 in archivio, il sistema provvede automaticamente a generare più versioni della stessa immagine, con
 dimensioni diverse definite in fase di configurazione del sistema. I redattori non devono
 preoccuparsi di scegliere la dimensione da utilizzare in un contenuto, in quanto la dimensione da
 utilizzare è definita nei Modelli di Contenuto.
 La pagina comprende le funzioni di ricerca, nella parte alta della pagina, e l'elenco delle Immagini in
 basso.
 Nell'elenco delle immagini è presentata un'anteprima dell'immagine insieme alla sua descrizione.
 Sono qui disponibili anche le icone per le funzioni “Dettaglio”, “Modifica” e “Elimina” immagine.

 .. figure:: _static/images/archivioImmagini.png
    :align: center
    :scale: 50 %
    :name: Archivio Immagini

    Fig. 16 - Archivio Immagini

2.6.1.1 *Ricerca Immagini*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 La ricerca base consente di specificare un testo da cercare nella descrizione delle immagini.
 Se si e f la ricerca, con un click su “Raffina la ricerca” sono mostrati ulteriori criteri di ricerca:

 **Gruppo:** consente di selezionare le sole Immagini appartenenti al gruppo scelto

 **Nome del file:** consente una ricerca sui nomi dei file delle immagini tramite una porzione di testo

 **Categoria:** per cercare le Immagini associate alla Categoria scelta

 La ricerca è avviata, in ogni caso, con un clic sul pulsante “Cerca”.
 Attenzione: la ricerca opera selezionando le Immagini che corrispondono contemporaneamente a
 tutti i criteri specificati (condizioni in AND).

2.6.1.2 *Dettaglio Immagine*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Con un click sull'icona “Dettaglio”, presente sotto ogni immagine, viene mostrato un elenco delle
 versioni dell'immagine: la dimensione originale e quelle generate automaticamente. Per ognuna di
 esse è possibile consultare la dimensione in pixel, la dimensione in KiloByte e, tramite un ulteriore
 click, vedere sul browser l'immagine in ciascuna dimensione (dopo la visione, usare il comando
 “indietro” del browser per tornare alla lista).

2.6.1.3 *Modifica Immagine*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Con un click sull'icona “Modifica”, presente sotto ogni immagine, si possono modificare tutti gli
 elementi dell'immagine, tranne il Gruppo proprietario. La funzione è analoga alla creazione di una
 nuova immagine.

2.6.1.4 *Elimina Immagine*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Con un click sull'icona “Elimina”, presente sopra ogni immagine, è possibile eliminare
 definitivamente l'immagine dall'archivio. Come al solito, l'operazione sarà impedita se l'immagine è
 utilizzata all'interno dei Contenuti, per evitare incongruenze.

2.6.1.5 *Nuova Immagine*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Tramite un click sul pulsante “Nuova” si accede alla pagina di caricamento di una nuova Immagine.
 I dati richiesti sono:

 **Descrizione:** si tratta di una descrizione ad uso interno, visibile solo nell'area di amministrazione

 **Gruppo:** è il gruppo proprietario dell'immagine, scelto fra queli associati all'utente che sta operando

 **File:** scegliere il file Immagine da caricare, sfogliando il disco locale. Il file dovrebbe avere un nome
 significativo. Il file deve essere in formato JPEG o PNG. Se l'immagine ha una risoluzione (e quindi
 una dimensione) eccessivi per l'uso su web, è opportuno provvedere preliminarmente ad una riduzione, intervenendo sul file tramite un qualunque programma di gestione di immagini o
 fotografie.

 **Normalizza nome file:** questa opzione abilita il sistema ad adattare il nome del file, ad esempio
 eliminando gli spazi.

 **Categorie:** è possibile associare una o più Categorie all'Immagine, scegliendo la categoria e cliccando
 sull'icona “associa”. Le Categorie associate sono elencate nella tabella sottostante, dalla quale è
 anche possibile effettuare la rimozione di una Categoria.

 Premendo il pulsante “Salva” inizia l'upload del file ed il suo inserimento in archivio.

 .. figure:: _static/images/nuovaImmagine.png
    :align: center
    :name: Nuova Immagine

    Fig. 17 - Nuova Immagine

2.6.2 Allegati
------------------
 Gli allegati sono documenti su file potenzialmente di qualunque tipo, che si intende mettere a
 disposizione degli utenti, tramite i Contenuti, senza che il sistema entri in alcun modo nel merito di
 ciò che contengono. Anche gli allegati dispongono di un archivio indipendente.
 La pagina comprende le funzioni di ricerca, nella parte alta della pagina, e l'elenco degli Allegati in
 basso.
 Nell'elenco degli Allegati è mostrata la descrizione di ogni allegato. Sono qui disponibili anche le
 icone per le funzioni “Scarica”, “Modifica” e “Elimina” allegato.

 .. figure:: _static/images/archivioAllegati.png
    :align: center
    :name: Archivio Allegati

    Fig. 18 - Archivio Allegati

2.6.2.1 *Ricerca Allegati*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 La ricerca base consente di specificare un testo da cercare nella descrizione degli allegati.
 Se si desidera raffinare la ricerca, con un click su “Raffina la ricerca” sono mostrati ulteriori criteri di
 ricerca:

 **Gruppo:** consente di selezionare i soli Allegati appartenenti al gruppo scelto

 **Nome del file:** consente una ricerca sui nomi dei file degli Allegati tramite una porzione di testo

 **Categoria:** per cercare gli Allegati associati alla Categoria scelta

 La ricerca è avviata, in ogni caso, con un clic sul pulsante “Cerca”.
 Attenzione: la ricerca opera selezionando gli Allegati che corrispondono contemporaneamente a
 tutti i criteri specificati (condizioni in AND).

2.6.2.2 *Scarica Allegato*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Con un click sull'icona “Scarica”, presente sotto ogni allegato, possibile scaricare (download)
 l'allegato. A seconda dl tipo di allegato e della configurazione del proprio browser e del proprio
 sistema operativo, è possibile che l'allegato sia riconosciuto e direttamente visualizzato sul browser.

2.6.2.3 *Modifica Allegato*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Con un click sull'icona “Modifica”, presente sotto ogni allegato, si possono modificare tutti gli
 elementi dell'allegato, tranne il Gruppo proprietario. La funzione è analoga alla creazione di un
 nuovo allegato.

2.6.2.4 *Elimina Allegato*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Con un click sull'icona “Elimina”, presente sopra ogni allegato ta, è possibile eliminare
 definitivamente l'allegato dall'archivio. Come al solito, l'operazione sarà impedita se l'allegato è
 utilizzato all'interno dei Contenuti, per evitare incongruenze.

2.6.2.5 *Nuovo Allegato*
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
 Tramite un click sul pulsante “Nuovo” si accede alla pagina di caricamento di un nuovo Allegato. I
 dati richiesti sono:

 **Descrizione:** si tratta di una descrizione ad uso interno, visibile solo nell'area di amministrazione

 **Gruppo:** è il gruppo proprietario dell'allegato, scelto fra quelli associati all'utente che sta operando

 **Categorie:** è possibile associare una o più Categorie all'allegato, scegliendo la categoria e cliccando
 sull'icona “associa”. Le Categorie associate sono elencate nella tabella sottostante, dalla quale è
 anche possibile effettuare la rimozione di una Categoria.

 **File:** scegliere il file da caricare, sfogliando il disco locale. Il file dovrebbe avere un nome significativo.
 Per motivi di sicurezza, i tipi di file da caricare, riconosciuto in base all'estensione, sono limitati
 (definiti in fase di realizzazione del sistema).

 **Normalizza nome file:** questa opzione abilita il sistema ad adattare il nome del file, ad esempio
 eliminando gli spazi.

 Premendo il pulsante “Salva” inizia l'upload del file ed il suo inserimento in archivio.

 .. figure:: _static/images/nuovoAllegato.png
    :align: center
    :name: Nuovo Allegato

    Fig. 19 - Nuovo Allegato
