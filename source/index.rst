Documentazione Entando
================================================

Lo scopo di questa guida è quello di fornire indicazioni sulla gestione dei contenuti per portali,
intranet realizzati con la piattaforma Entando, descrivendo le funzionalità e le modalità d'uso delle
funnzioni di editing fornite dalla piattaforma.
Il documento non include informazioni sullo sviluppo e manutenzione del sistema, trattati in altre
guide specifiche.

Questa guida è divisa in due parti.
La prima parte illustra in maniera discorsiva i concetti di base della Piattaforma Entando, per fornire
un quadro organico del sistema.
La seconda parte illustra le singole funzioni così come sono disponibili agli utenti nell'area di
amministrazione, fornendo le indicazioni operative.


Per maggiori approfondimenti sulla piattaforma Entando visitate: https://dev.entando.org che contiene tutte le risorse ed informazioni dettagliate per gli sviluppatori.

Contenuti:
^^^^^^^^^^^^^

.. toctree::
   :maxdepth: 3

   entandoPlatform
   adminArea

