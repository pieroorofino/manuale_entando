.. entandoDocumentation documentation master file, created by
   sphinx-quickstart on Mon Jun 15 12:43:28 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Entando's documentation!
================================================
Contents:
Lo scopo di questa guida è quello di fornire indicazioni sulla gestione dei contenuti per portali,
intranet realizzati con la piattaforma Entando, descrivendo le funzionalità e le modalità d'uso delle
funnzioni di editing fornite dalla piattaforma.
Il documento non include informazioni sullo sviluppo e manutenzione del sistema, trattati in altre
guide specifiche.

.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
* :ref:`entandoPlatform`
